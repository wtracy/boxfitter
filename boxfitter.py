from collections import deque


def findLargestSquare(grid):
  '''Returns a Box object representing the largest square that can fit entirely
  inside the given grid without oberlapping any obstacles in the grid. Entries
  in the grid with the value True are obstacles, and those with the value False
  are free.'''
  targets = deque()
  start = (0, 0)
  targets.append(start)
  seen = set()

  best = Box(start)

  # Go until we have drained the queue. _processNext() adds new candidates to
  # the end of the queue as it finds them.
  while len(targets) > 0:
    candidate = _processNext(grid, targets, seen)
    if candidate.size > best.size:
      best = candidate

  return best

def _processNext(grid, targets, seen):
  '''Pops the leftmost candidate off the queue and tests it. Returns a Box
  representing the largest legal square that can be placed with the upper-left
  corner at the discovered target. Appends new targets to the right end of the
  queue as they are discovered.'''
  target = targets.popleft()
  box = Box(target)

  if target not in seen:
    seen.add(target)
    box.grow(grid, targets)

  return box

class Box:
  def __init__(self, coords):
    (self.x, self.y) = coords
    self.size = 0

  def grow(self, grid, targets):
    '''Finds the largest square that can exist with the upper-left corner at the location assigned by the constructor, and updates its size accordingly.'''
    while self._canGrow(grid, targets):
      self.size = self.size + 1

  def _canGrow(self, grid, targets):
    '''Returns True if the square can be expanded by one unit down and to the
    left without overlapping an obstacle or extending beyond the edge of the
    grid. Returns False otherwise. Appends new targets to the queue as they are
    discovered'''
    if not _isInGrid(self.x+self.size, self.y+self.size, grid):
      return False
    if not self._rightEdgeIsClear(grid, targets):
      return False
    if not self._bottomEdgeIsClear(grid, targets):
      return False
    if not self._cornerIsClear(grid, targets):
      return False
    return True

  def _rightEdgeIsClear(self, grid, targets):
    '''Returns True if the square can be expanded one unit to the right without
    overlapping an obstacle. Returns False otherwise. Adds new targets to the
    queue as they are discovered.'''
    x = self.x + self.size

    # Iterate up right side so we can stop at the lowermost obstacle.
    for y in range(self.y + self.size - 1, self.y - 1, -1):
      if grid[x][y]:
        _spawnNewTargets(self.x, self.y, x, y, grid, targets)
        return False
    return True

  def _bottomEdgeIsClear(self, grid, targets):
    '''Returns True if the square can be expanded one unit down without
    overlapping an obstacle. Returns False otherwise. Adds new targets to the
    queue as they are discovered.'''
    y = self.y + self.size

    # Iterate across bottom from right to left, so we can stop at the rightmost obstacle.
    for x in range(self.x + self.size - 1, self.x - 1, -1):
      if grid[x][y]:
        _spawnNewTargets(self.x, self.y, x, y, grid, targets)
        return False
    return True

  def _cornerIsClear(self, grid, targets):
    '''Returns True if the cell immediately below and to the right of the
    square is free from obstacles. Returns False otherwise. Adds new targets to the
    queue as they are discovered.'''
    x = self.x + self.size
    y = self.y + self.size

    if grid[x][y]:
      _spawnNewTargets(self.x, self.y, x, y, grid, targets)
      return False
    return True

def _isInGrid(x, y, grid):
  '''Returns False if the provided coordinates fall past the bottom or right
  edge of the grid. Returns True otherwise.'''
  if x >= len(grid):
    return False
  if y >= len(grid[0]):
    return False
  return True

def _spawnNewTargets(startX, startY, x, y, grid, targets):
  '''Creates two new targets that avoid any obstacle at the location x, y and
  appends them to the right end of the queue. One sits just to the right of the
  obstacle, starting at the same Y coordinate as the previous target. The other
  sits just below the obstacle, starting at the same X coordinate as the
  previous target.'''
  targets.append( (x + 1, startY) )
  targets.append( (startX, y + 1) )
