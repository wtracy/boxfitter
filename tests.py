import unittest

import boxfitter

class TestFitter(unittest.TestCase):
  def testZeroGrid(self):
    grid = [[]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 0)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 0)

  def testEmptyGrid(self):
    grid = [[False, False], [False, False]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 2)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 0)

  def testGridBottomRightCornerObstacle(self):
    grid = [[False, False], [False, True]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 1)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 0)

  def testGridRightEdgeObstacle(self):
    grid = [[False, False], [True, False]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 1)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 0)

  def testGridBottomEdgeObstacle(self):
    grid = [[False, True], [False, False]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 1)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 0)

  def testGridTopLeftObstacle(self):
    grid = [[True, False], [False, False]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 1)
    self.assertEqual(result.x, 1)
    self.assertEqual(result.y, 0)

  # not yet walked through
  def testGridFull(self):
    grid = [[True, True], [True, True]]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 0)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 0)

  def testGridSkipCorner(self):
    grid = [
      [False, False, False, False],
      [False, True, False, False],
      [False, False, False, False],
      [False, False, False, False]
    ]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 2)
    self.assertEqual(result.x, 2)
    self.assertEqual(result.y, 0)

  def testGridSkipRight(self):
    grid = [
      [False, False, False, False],
      [True, False, False, False],
      [False, False, False, False],
      [False, False, False, False]
    ]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 3)
    self.assertEqual(result.x, 0)
    self.assertEqual(result.y, 1)

  def testGridSkipDown(self):
    grid = [
      [False, True, False, False],
      [False, False, False, False],
      [False, False, False, False],
      [False, False, False, False]
    ]
    result = boxfitter.findLargestSquare(grid)
    self.assertEqual(result.size, 3)
    self.assertEqual(result.x, 1)
    self.assertEqual(result.y, 0)

if __name__ == '__main__':
    unittest.main()

